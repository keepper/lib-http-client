<?php
namespace Keepper\Lib\HttpClient\Tests;

use Keepper\Lib\Curl\CurlInterface;
use Keepper\Lib\Curl\Error\CurlError;
use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\Lib\Curl\Option\OptionInterface;
use Keepper\Lib\HttpClient\Exceptions\ConfigurationException;
use Keepper\Lib\HttpClient\Exceptions\ConnectException;
use Keepper\Lib\HttpClient\Exceptions\ConnectTimeoutException;
use Keepper\Lib\HttpClient\Exceptions\CurlErrorMapper;
use Keepper\Lib\HttpClient\Exceptions\RequestException;
use Keepper\Lib\HttpClient\Exceptions\RequestTimeoutException;
use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Keepper\Lib\HttpClient\HttpClient;

class HttpClientTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject
	 */
	private $curl;

	public function setUp() {
		parent::setUp();
		$this->curl = $this->getMockBuilder(CurlInterface::class)->getMock();
	}

	/**
	 * @dataProvider dataProviderForProxyForSetOption
	 */
	public function testProxyForSetOption($method, $expectedOption, $value) {
		$client = new HttpClient($this->curl);
		// Проверяем что вызывается необходимый метод
		$this->curl
			->method('setOption')
			->with($expectedOption, $value);

		$result = $client->$method($value);
		$this->assertEquals($client, $result);
	}

	public function dataProviderForProxyForSetOption() {
		return [
			['withReferer', OptionInterface::REFERER, 'http://php.net'],
			['withReferer', OptionInterface::REFERER, 'http://php.net/manual/ru/'],
			['withUserAgent', OptionInterface::USERAGENT, 'test-user-agent-'.rand(1,100)],
			['fromNetworkInterface', OptionInterface::INTERFACE, 'eth'.rand(1,2)]
		];
	}

	/**
	 * @dataProvider dataProviderForFollowLocation
	 */
	public function testFollowLocation($autoReferer, $maxRedirects, $mask, $sendAuth, $protocols, $expectedOptions) {
		$client = new HttpClient($this->curl);
		$this->curl
			->method('setOptions')
			->with($expectedOptions);

		$result = $client->followLocation($autoReferer, $maxRedirects, $mask, $sendAuth, $protocols);
		$this->assertEquals($client, $result);
	}

	public function dataProviderForFollowLocation() {
		return [
			[true, null, null, null, null, [OptionInterface::FOLLOWLOCATION => true, OptionInterface::AUTOREFERER => true]],
			[false, 1, null, null, null, [OptionInterface::FOLLOWLOCATION => true, OptionInterface::AUTOREFERER => false, OptionInterface::MAXREDIRS => 1]],
			[true, 2, 1, null, null, [OptionInterface::FOLLOWLOCATION => true, OptionInterface::AUTOREFERER => true, OptionInterface::MAXREDIRS => 2, OptionInterface::POSTREDIR => 1]],
			[true, 2, 3, true, null, [OptionInterface::FOLLOWLOCATION => true, OptionInterface::AUTOREFERER => true, OptionInterface::MAXREDIRS => 2, OptionInterface::POSTREDIR => 3, OptionInterface::UNRESTRICTED_AUTH => true]],
			[true, 2, 3, false, CURLPROTO_HTTP, [OptionInterface::FOLLOWLOCATION => true, OptionInterface::AUTOREFERER => true, OptionInterface::MAXREDIRS => 2, OptionInterface::POSTREDIR => 3, OptionInterface::UNRESTRICTED_AUTH => false, OptionInterface::REDIR_PROTOCOLS => CURLPROTO_HTTP]],
		];
	}

	/**
	 * @dataProvider dataProviderForWithTimeOut
	 */
	public function testWithTimeOut($method, $request, $connect, $expectedOptions) {
		$client = new HttpClient($this->curl);
		$this->curl
			->method('setOptions')
			->with($expectedOptions);

		$result = $client->$method($request, $connect);
		$this->assertEquals($client, $result);
	}

	public function dataProviderForWithTimeOut() {
		return [
			['withTimeOut', 10, null, [OptionInterface::TIMEOUT => 10]],
			['withTimeOut', 20, 5, [OptionInterface::TIMEOUT => 20, OptionInterface::CONNECTTIMEOUT => 5]],
			['withTimeOutMs', 10, null, [OptionInterface::TIMEOUT_MS => 10]],
			['withTimeOutMs', 20, 5, [OptionInterface::TIMEOUT_MS => 20, OptionInterface::CONNECTTIMEOUT_MS => 5]],
		];
	}

	/**
	 * @dataProvider dataProviderForException
	 */
	public function testException($errNo, $message, $expectedException) {
		$client = new HttpClient($this->curl);

		$this->curl
			->method('error')
			->with()
			->willReturn(new CurlError($errNo, $message));

		$this->curl->method('exec')->willThrowException(new CurlException('Some error'));

		try {
			$client->get('http://some.domain/');
			$this->assertTrue(false, 'Ожидали исключение');
		} catch (CurlException $e) {
			$this->assertEquals($errNo, $e->getCode(), 'Ожидали, что у исключения в коде ошибки будет высавлен код ошибки curl');
			$this->assertInstanceOf($expectedException, $e, 'Ожидали исключение '.$expectedException);
		}
	}

	public function dataProviderForException() {
		$mapper = new CurlErrorMapper();
		$cases = [];
		foreach ($mapper->getConfigurationCodes() as $code) {
			$cases[] = [$code, 'Some configuration error with code '.$code, ConfigurationException::class];
		}
		foreach ($mapper->getConnectCodes() as $code) {
			$cases[] = [$code, 'Some connection error with code '.$code, ConnectException::class];
		}
		foreach ($mapper->getConnectTimeoutCodes() as $code) {
			$cases[] = [$code, 'Some connection timeout error with code '.$code, ConnectTimeoutException::class];
		}
		foreach ($mapper->getRequestCodes() as $code) {
			$cases[] = [$code, 'Some request error with code '.$code, RequestException::class];
		}
		$cases[] = [-123, 'Some unknown error with code '.$code, RequestException::class];
		foreach ($mapper->getRequestTimeoutCodes() as $code) {
			$cases[] = [$code, 'Some request timeout error with code '.$code, RequestTimeoutException::class];
		}
		foreach ($mapper->getResponseFormatCodes() as $code) {
			$cases[] = [$code, 'Some response format error with code '.$code, ResponseFormatException::class];
		}


		return $cases;
	}
}