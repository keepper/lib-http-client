<?php
namespace Keepper\Lib\HttpClient\Tests;

use Keepper\Lib\HttpClient\Exceptions\ConnectException;
use Keepper\Lib\HttpClient\Exceptions\ConnectTimeoutException;
use Keepper\Lib\HttpClient\Exceptions\RequestTimeoutException;
use Keepper\Lib\HttpClient\HttpClient;
use Monolog\Logger;

class HttpClientIntegrationTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @group integration
	 */
	public function test200Ok() {
		$logger = new Logger('Unit tests');
		$client = new HttpClient(null, $logger);
		$client->withTimeOut(2, 1);

		try {
			$response = $client->get('http://php.net/manual/ru/');
		} catch (ConnectTimeoutException $e) {
			$this->markTestSkipped('Connection timeout of request');
		} catch (ConnectException $e) {
			$this->markTestSkipped('Connection error');
		}

		$this->assertEquals(200, $response->getStatusCode(), 'Ожидали ответ 200 OK');
		$this->assertEquals('text/html; charset=utf-8', $response->getHeader('Content-Type')[0]);
		$startContent = substr((string) $response->getBody(), 0, 15);
		$this->assertTrue('HTTP/1.1 200 OK' != $startContent, 'Не ожидали в теле заголовков');
	}

	/**
	 * @group integration
	 */
	public function test404() {
		$logger = new Logger('Unit tests');
		$client = new HttpClient(null, $logger);
		$client->withTimeOut(2, 1);

		try {
			$response = $client->get('http://php.net/manual/asasa/');
		} catch (ConnectTimeoutException $e) {
			$this->markTestSkipped('Connection timeout of request');
		} catch (ConnectException $e) {
			$this->markTestSkipped('Connection error');
		} catch (RequestTimeoutException $e) {
			$this->markTestSkipped('Request timeout');
		}

		$this->assertEquals(404, $response->getStatusCode(), 'Ожидали ответ 404');
		$this->assertEquals('text/html; charset=utf-8', $response->getHeader('Content-Type')[0]);
	}

	/**
	 * @group integration
	 */
	public function test301() {
		$logger = new Logger('Unit tests');
		$client = new HttpClient(null, $logger);
		$client->withTimeOut(2, 1);

		try {
			$response = $client->get('http://www.php.net/manual/ru/');
		} catch (ConnectTimeoutException $e) {
			$this->markTestSkipped('Connection timeout of request');
		} catch (ConnectException $e) {
			$this->markTestSkipped('Connection error');
		} catch (RequestTimeoutException $e) {
			$this->markTestSkipped('Request timeout');
		}

		$this->assertEquals(301, $response->getStatusCode(), 'Ожидали ответ 301');
		$this->assertEquals('text/html', $response->getHeader('Content-Type')[0]);
	}

	/**
	 * @group integration
	 */
	public function testFollowLocation() {
		$logger = new Logger('Unit tests');
		$client = new HttpClient(null, $logger);
		$client->followLocation(true, 3, 3);
		$client->withTimeOut(2, 1);

		try {
			$response = $client->get('http://www.php.net/manual/ru/');
		} catch (ConnectTimeoutException $e) {
			$this->markTestSkipped('Connection timeout of request');
		} catch (ConnectException $e) {
			$this->markTestSkipped('Connection error');
		} catch (RequestTimeoutException $e) {
			$this->markTestSkipped('Request timeout');
		}

		$this->assertEquals(200, $response->getStatusCode(), 'Ожидали ответ 200');
		$this->assertEquals('text/html; charset=utf-8', $response->getHeader('Content-Type')[0]);
	}
}