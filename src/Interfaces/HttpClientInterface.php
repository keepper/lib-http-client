<?php
namespace Keepper\Lib\HttpClient\Interfaces;

use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\Lib\HttpClient\Exceptions\ConfigurationException;
use Keepper\Lib\HttpClient\Exceptions\ConnectException;
use Keepper\Lib\HttpClient\Exceptions\ConnectTimeoutException;
use Keepper\Lib\HttpClient\Exceptions\RequestException;
use Keepper\Lib\HttpClient\Exceptions\RequestTimeoutException;
use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface {

	/**
	 * Выполняет GET запрос на указанный URL, для передачи Параметров в строке URL
	 * используйте $queryParameters как ассоциативный масив, где ключем является имя параметра
	 * значением соответственно его значение
	 *
	 * @param $url
	 * @param array|null $queryParameters
	 * @return ResponseInterface
	 *
	 * @throws CurlException
	 * @throws ConfigurationException
	 * @throws ConnectException
	 * @throws ConnectTimeoutException
	 * @throws RequestException
	 * @throws RequestTimeoutException
	 * @throws ResponseFormatException
	 */
	public function get(string $url, array $queryParameters = null): ResponseInterface;

	/**
	 * Выполняет POST запрос на указанный URL
	 * @param string $url
	 * @param array|null $postParameters
	 * @param array|null $files             Ассоциативный масив, где в качестве ключей, находятся имена передаваемых полей,
	 *                                      а в качестве значений пати до файлов
	 * @return ResponseInterface
	 *
	 * @throws CurlException
	 * @throws ConfigurationException
	 * @throws ConnectException
	 * @throws ConnectTimeoutException
	 * @throws RequestException
	 * @throws RequestTimeoutException
	 * @throws ResponseFormatException
	 */
	public function post(string $url, array $postParameters = null, array $files = null): ResponseInterface;

	/**
	 * Выполняет PUT запрос на указанный URL
	 * @param string $url
	 * @param array|null $postParameters
	 * @return ResponseInterface
	 *
	 * @throws CurlException
	 * @throws ConfigurationException
	 * @throws ConnectException
	 * @throws ConnectTimeoutException
	 * @throws RequestException
	 * @throws RequestTimeoutException
	 * @throws ResponseFormatException
	 */
	public function put(string $url, array $postParameters = null): ResponseInterface;
}