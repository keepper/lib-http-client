<?php
namespace Keepper\Lib\HttpClient\Interfaces;

interface ConfiguredHttpClientInterface extends HttpClientInterface {

	/**
	 * Конфигурирует объект, добавляя значение Поля заголовка referer которое будет использоваться
	 * при исполнении запросов
	 *
	 * @param string $referer
	 * @return ConfiguredHttpClientInterface
	 */
	public function withReferer(string $referer): ConfiguredHttpClientInterface;

	/**
	 * @param string $userAgent
	 * @return ConfiguredHttpClientInterface
	 */
	public function withUserAgent(string $userAgent): ConfiguredHttpClientInterface;

	/**
	 * Устанавливает параметры timeout ограничения ожидания исполнения в секундах.
	 * Для бесконечного ожидания используйте 0 вместо значения
	 * @param int $timeout              Максимально позволенное количество секунд для выполнения cURL-функций.
	 * @param int|null $connectTimeout  Количество секунд ожидания при попытке соединения.
	 * @return mixed
	 */
	public function withTimeOut(int $timeout = null, int $connectTimeout = null): ConfiguredHttpClientInterface;

	/**
	 * Устанавливает параметры timeout ограничения ожидания исполнения в милисекундах.
	 * Для бесконечного ожидания используйте 0 вместо значения
	 * @param int $timeout              Максимально позволенное количество секунд для выполнения cURL-функций.
	 * @param int|null $connectTimeout  Количество секунд ожидания при попытке соединения.
	 * @return mixed
	 */
	public function withTimeOutMs(int $timeout = null, int $connectTimeout = null): ConfiguredHttpClientInterface;

	/**
	 * Говорит, что запросы должны идти через указанный сетевой интерфейс. Может быть именем интерфейса, IP адресом или именем хоста.
	 * (для серверов с несколькими сетевыми интерфейсами)
	 * @param string $interface
	 * @return ConfiguredHttpClientInterface
	 */
	public function fromNetworkInterface(string $interface): ConfiguredHttpClientInterface;

	/**
	 * Заставляет библиотеку реагировать на заголовки Location: (редиректы)
	 * В случае необходимости ограничения кол-ва редиректов, используйте параметр $maxRedirectss
	 * @param   bool        $autoReferer    TRUE для автоматической установки поля Referer: в запросах, перенаправленных заголовком Location:.
	 * @param   int|null    $maxRedirects   Ограничение кол-ва редиректов при исполнении запроса
	 * @param   int|null    $mask           Битовая маска, содержащая 1 (301 Moved Permanently), 2 (302 Found) и 4 (303 See Other),
	 *                                      чтобы задавать должен ли метод HTTP POST если произошел указанный тип перенаправления.
	 * @param   bool|null   $sendAuth       TRUE для продолжения отправки логина и пароля при редиректах, даже при изменении имени хоста.
	 * @param   int|null    $protocols      Битовая маска из значений CURLPROTO_*. Данная битовая масска ограничивает протоколы,
	 *                                      используемые libcurl при редиректе. Это позволяет ограничить набор используемых
	 *                                      протоколов при редиректах для некоторых передач.
	 *                                      По умолчанию, libcurl поддерживает все протоколы, кроме FILE и SCP.
	 *                                      В версиях, предшествовавших 7.19.4, перенаправление использовалось для всех
	 *                                      протоколов без исключения.
	 * @return ConfiguredHttpClientInterface
	 */
	public function followLocation(
		bool $autoReferer = true,
		int $maxRedirects = null,
		int $mask = null,
		bool $sendAuth = null,
		int $protocols = null
	): ConfiguredHttpClientInterface;

}