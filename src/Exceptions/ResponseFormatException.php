<?php
namespace Keepper\Lib\HttpClient\Exceptions;

/**
 * Исключение генерируется при получения не понятного ответа от удаленного узла
 * Соединения и запрос прошли успешно, ответ получен, но он в не ожидаемом формате
 */
class ResponseFormatException extends RequestException {

}