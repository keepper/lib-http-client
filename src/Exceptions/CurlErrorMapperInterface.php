<?php
namespace Keepper\Lib\HttpClient\Exceptions;

use Keepper\Lib\Curl\Error\CurlErrorInterface;
use Keepper\Lib\Curl\Exceptions\CurlException;

/**
 * Интерфейс обеспечивающий мапинг кодов ошибок curl в систему исключений данной библиотеки
 *
 * @see CurlException               Базовое исключение от которого наследуются все остальные
 * @see ConfigurationException      Исключение говорящее о том, что запрос не был исполнен (не было попытки даже коннекта)
 *                                  так как в текущей конфигурации десткриптов curl есть ошибка
 * @see ConnectException            Запрос прервался на этапе установления соединения
 * @see ConnectTimeoutException     Запрос прервался на этапе установления соединения, по причине обрыва по timeout'у
 * @see RequestException            Запрос либо был преван либо исполнен с ошибкой
 * @see RequestTimeoutException     Запрос прервался, по причине обрыва по timeout'у, после установления соединения
 * @see ResponseFormatException     Запрос исполнен, но получен не корректный ответ от сервера
 */
interface CurlErrorMapperInterface {

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see ConfigurationException
	 * @return array
	 */
	public function getConfigurationCodes(): array;

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see ConnectException
	 * @return array
	 */
	public function getConnectCodes(): array;

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see ConnectTimeoutException
	 * @return array
	 */
	public function getConnectTimeoutCodes(): array;

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see RequestException
	 * @return array
	 */
	public function getRequestCodes(): array;

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see RequestTimeoutException
	 * @return array
	 */
	public function getRequestTimeoutCodes(): array;

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see ResponseFormatException
	 * @return array
	 */
	public function getResponseFormatCodes(): array;

	/**
	 * Генерирует исключение по коду ошибки @see curl_errno()
	 * @param CurlErrorInterface $error
	 * @return mixed
	 *
	 * @throws CurlException
	 * @throws ConfigurationException
	 * @throws ConnectException
	 * @throws ConnectTimeoutException
	 * @throws RequestException
	 * @throws RequestTimeoutException
	 * @throws ResponseFormatException
	 */
	public function throwExceptionByError(CurlErrorInterface $error);
}