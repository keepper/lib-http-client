<?php
namespace Keepper\Lib\HttpClient\Exceptions;

use Keepper\Lib\Curl\Error\CurlErrorInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class CurlErrorMapper implements CurlErrorMapperInterface {

	use LoggerAwareTrait;

	public function __construct(
		LoggerInterface $logger = null
	) {
		$this->setLogger($logger ?? new NullLogger());
	}

	/**
	 * @inheritdoc
	 */
	public function throwExceptionByError(CurlErrorInterface $error) {

		if (in_array($error->code(), $this->getConfigurationCodes())) {
			throw new ConfigurationException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		if (in_array($error->code(), $this->getConnectCodes())) {
			throw new ConnectException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		if (in_array($error->code(), $this->getConnectTimeoutCodes())) {
			throw new ConnectTimeoutException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		if (in_array($error->code(), $this->getRequestCodes())) {
			throw new RequestException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		if (in_array($error->code(), $this->getRequestTimeoutCodes())) {
			throw new RequestTimeoutException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		if (in_array($error->code(), $this->getResponseFormatCodes())) {
			throw new ResponseFormatException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());
		}

		// Так как точно не ясно получил ли сервер запрос или нет, считаем, что до нег он мог дойти
		throw new RequestException($error->message() . "\nByCode: (" . $error->code() . ") " . $error->messageByCode(), $error->code());

	}

	/**
	 * @inheritdoc
	 */
	public function getConfigurationCodes(): array {
		return [
			CurlErrorInterface::UNSUPPORTED_PROTOCOL,
			CurlErrorInterface::FAILED_INIT,
			CurlErrorInterface::URL_MALFORMAT,
			CurlErrorInterface::FTP_PORT_FAILED,
			CurlErrorInterface::FTP_COULDNT_USE_REST,
			CurlErrorInterface::FILE_COULDNT_READ_FILE,
			CurlErrorInterface::BAD_FUNCTION_ARGUMENT,
			CurlErrorInterface::INTERFACE_FAILED,
			CurlErrorInterface::UNKNOWN_OPTION,
			CurlErrorInterface::TELNET_OPTION_SYNTAX,
			CurlErrorInterface::SSL_ENGINE_NOTFOUND,
			CurlErrorInterface::SSL_ENGINE_SETFAILED,
			CurlErrorInterface::SSL_CERTPROBLEM,
			CurlErrorInterface::SSL_CIPHER,
			CurlErrorInterface::LDAP_INVALID_URL,
			CurlErrorInterface::FILESIZE_EXCEEDED,
			CurlErrorInterface::SSL_CACERT_BADFILE,
			CurlErrorInterface::SSL_CRL_BADFILE,
			CurlErrorInterface::SSL_ENGINE_INITFAILED,
			CurlErrorInterface::USE_SSL_FAILED,
			CurlErrorInterface::SSL_PINNEDPUBKEYNOTMATCH
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getConnectCodes(): array {
		return [
			CurlErrorInterface::COULDNT_RESOLVE_PROXY,
			CurlErrorInterface::COULDNT_RESOLVE_HOST,
			CurlErrorInterface::COULDNT_CONNECT,
			CurlErrorInterface::FTP_CANT_GET_HOST,
			CurlErrorInterface::SSL_CONNECT_ERROR,
			CurlErrorInterface::LDAP_CANNOT_BIND
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getConnectTimeoutCodes(): array {
		return [
			CurlErrorInterface::FTP_ACCEPT_TIMEOUT
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getRequestCodes(): array {
		return [
			CurlErrorInterface::FTP_COULDNT_SET_TYPE,
			CurlErrorInterface::FTP_COULDNT_RETR_FILE,
			CurlErrorInterface::QUOTE_ERROR,
			CurlErrorInterface::HTTP_RETURNED_ERROR,
			CurlErrorInterface::WRITE_ERROR,
			CurlErrorInterface::UPLOAD_FAILED,
			CurlErrorInterface::READ_ERROR,
			CurlErrorInterface::OUT_OF_MEMORY,
			CurlErrorInterface::RANGE_ERROR,
			CurlErrorInterface::HTTP_POST_ERROR,
			CurlErrorInterface::BAD_DOWNLOAD_RESUME,
			CurlErrorInterface::LDAP_SEARCH_FAILED,
			CurlErrorInterface::FUNCTION_NOT_FOUND,
			CurlErrorInterface::ABORTED_BY_CALLBACK,
			CurlErrorInterface::TOO_MANY_REDIRECTS,
			CurlErrorInterface::GOT_NOTHING,
			CurlErrorInterface::SSH,
			CurlErrorInterface::AGAIN,
			CurlErrorInterface::SSL_SHUTDOWN_FAILED,
			CurlErrorInterface::CONV_REQD,
			CurlErrorInterface::REMOTE_FILE_NOT_FOUND,
			CurlErrorInterface::CONV_FAILED,
			CurlErrorInterface::TFTP_ILLEGAL,
			CurlErrorInterface::TFTP_UNKNOWNID,
			CurlErrorInterface::REMOTE_FILE_EXISTS,
			CurlErrorInterface::REMOTE_DISK_FULL,
			CurlErrorInterface::TFTP_PERM,
			CurlErrorInterface::TFTP_NOTFOUND,
			CurlErrorInterface::LOGIN_DENIED,
			CurlErrorInterface::SEND_FAIL_REWIND,
			CurlErrorInterface::PEER_FAILED_VERIFICATION,
			CurlErrorInterface::SSL_ISSUER_ERROR,
			CurlErrorInterface::SSL_INVALIDCERTSTATUS,
			CurlErrorInterface::HTTP2_STREAM,
			CurlErrorInterface::RECURSIVE_API_CALL
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getRequestTimeoutCodes(): array {
		return [
			CurlErrorInterface::OPERATION_TIMEDOUT,
			CurlErrorInterface::SEND_ERROR,
			CurlErrorInterface::RECV_ERROR
		];
	}

	/**
	 * Возвращает список кодов ошибок @see curl_errno() который преобразуюся в @see ResponseFormatException
	 * @return array
	 */
	public function getResponseFormatCodes(): array {
		return [
			CurlErrorInterface::FTP_WEIRD_SERVER_REPLY,
			CurlErrorInterface::FTP_WEIRD_PASS_REPLY,
			CurlErrorInterface::FTP_WEIRD_PASV_REPLY,
			CurlErrorInterface::FTP_WEIRD_227_FORMAT,
			CurlErrorInterface::HTTP2,
			CurlErrorInterface::PARTIAL_FILE,
			CurlErrorInterface::BAD_CONTENT_ENCODING,
			CurlErrorInterface::FTP_PRET_FAILED
		];
	}
}