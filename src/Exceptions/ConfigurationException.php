<?php
namespace Keepper\Lib\HttpClient\Exceptions;

use Keepper\Lib\Curl\Exceptions\CurlException;

/**
 * Исключение генерируемое, в момент попытки исполнения запроса, но гарантировано, говорит, что запроса не производилось,
 * так как в параметрах curl есть ошибки конфигурации
 */
class ConfigurationException extends CurlException {

}