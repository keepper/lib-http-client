<?php
namespace Keepper\Lib\HttpClient\Exceptions;

use Keepper\Lib\Curl\Exceptions\CurlException;

/**
 * Исключение генерируемое при проблеме исполнения запроса к удаленному узлу, но после установления соединения
 */
class RequestException extends CurlException {

}