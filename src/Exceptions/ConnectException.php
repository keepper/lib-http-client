<?php
namespace Keepper\Lib\HttpClient\Exceptions;

use Keepper\Lib\Curl\Exceptions\CurlException;

/**
 * Исключение генерируемое при проблемах соединения с удаленным узлом
 */
class ConnectException extends CurlException {

}