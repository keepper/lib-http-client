<?php
namespace Keepper\Lib\HttpClient\Exceptions;

/**
 * Исключение генерируемое при таймауте соединения с сетевым адресом
 */
class ConnectTimeoutException extends ConnectException {

}