<?php
namespace Keepper\Lib\HttpClient;

use Keepper\Lib\Curl\Curl;
use Keepper\Lib\Curl\CurlInterface;
use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\Lib\Curl\Option\OptionInterface;
use Keepper\Lib\HttpClient\Exceptions\ConfigurationException;
use Keepper\Lib\HttpClient\Exceptions\CurlErrorMapper;
use Keepper\Lib\HttpClient\Exceptions\CurlErrorMapperInterface;
use Keepper\Lib\HttpClient\Exceptions\RequestException;
use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Phly\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class HttpClient implements ConfiguredHttpClientInterface {

	use LoggerAwareTrait;

	/**
	 * @var CurlInterface
	 */
	protected $curl;

	/**
	 * @var CurlErrorMapperInterface
	 */
	protected $errorMapper;

	private $canToggleCurlLogger = false;

	public function __construct(
		CurlInterface $curl     = null,
		LoggerInterface $logger = null
	) {
		$this->setLogger($logger ?? new NullLogger());
		if ( is_null($curl) ) {
			$this->canToggleCurlLogger = true;
		}
		$this->curl = $curl ?? new Curl(null, $this->logger);
		$this->setErrorMapper(new CurlErrorMapper($this->logger));

		$this->curl->init();
		$this->curl->setOption(OptionInterface::RETURNTRANSFER, true);
		$this->curl->setOption(OptionInterface::HEADER, true);
		$this->curl->setOption(OptionInterface::HEADER_OUT, true);
		$this->withUserAgent('Keepper-lib-http-client');
	}

	public function setErrorMapper(CurlErrorMapperInterface $errorMapper) {
		$this->errorMapper = $errorMapper;
	}

	/**
	 * @inheritdoc
	 */
	public function withReferer(string $referer): ConfiguredHttpClientInterface {
		$this->curl->setOption(OptionInterface::REFERER, $referer);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function withUserAgent(string $userAgent): ConfiguredHttpClientInterface {
		$this->curl->setOption(OptionInterface::USERAGENT, $userAgent);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function followLocation(
		bool $autoReferer = true,
		int $maxRedirects = null,
		int $mask = null,
		bool $sendAuth = null,
		int $protocols = null
	): ConfiguredHttpClientInterface {
		$options = [
			OptionInterface::FOLLOWLOCATION => true,
			OptionInterface::AUTOREFERER => $autoReferer
		];

		if ( !is_null($maxRedirects) ) {
			$options[OptionInterface::MAXREDIRS] = $maxRedirects;
		}

		if ( !is_null($mask) ) {
			$options[OptionInterface::POSTREDIR] = $mask;
		}

		if ( !is_null($sendAuth) ) {
			$options[OptionInterface::UNRESTRICTED_AUTH] = $sendAuth;
		}

		if ( !is_null($protocols) ) {
			$options[OptionInterface::REDIR_PROTOCOLS] = $protocols;
		}

		$this->curl->setOptions($options);
		return $this;
	}


	/**
	 * @inheritdoc
	 */
	public function withTimeOut(int $timeout = null, int $connectTimeout = null): ConfiguredHttpClientInterface {
		$options = [];
		if ( !is_null($timeout) ) {
			$options[OptionInterface::TIMEOUT] = $timeout;
		}

		if ( !is_null($connectTimeout) ) {
			$options[OptionInterface::CONNECTTIMEOUT] = $connectTimeout;
		}

		$this->curl->setOptions($options);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function withTimeOutMs(int $timeout = null, int $connectTimeout = null): ConfiguredHttpClientInterface {
		$options = [];
		if ( !is_null($timeout) ) {
			$options[OptionInterface::TIMEOUT_MS] = $timeout;
		}

		if ( !is_null($connectTimeout) ) {
			$options[OptionInterface::CONNECTTIMEOUT_MS] = $connectTimeout;
		}
		$this->curl->setOptions($options);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function fromNetworkInterface(string $interface): ConfiguredHttpClientInterface {
		$this->curl->setOption(OptionInterface::INTERFACE, $interface);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function get(string $url, array $queryParameters = null): ResponseInterface {
		$query = '';
		if ( !is_null($queryParameters) ) {
			foreach ($queryParameters as $queryParameter => $value) {
				//$query .= '&'.$queryParameter.'='.$this->curl->escape($value);
                $query .= '&'.$queryParameter.'='.$value;
			}
			$query = '?'.substr($query, 1);
		}
		$this->curl->setOption(OptionInterface::HTTPGET, true);
		return $this->doRequest($url . $query);
	}

	/**
	 * @inheritdoc
	 */
	public function post(string $url, array $postParameters = null, array $files = null): ResponseInterface {
		$postParameters = $postParameters ?? [];
		if ( !is_null($files) ) {
			foreach ($files as $name => $path) {
				if ( !file_exists($path) ) {
					throw new ConfigurationException('Указанный файл не существует ('.$name.') '.$path.'"');
				}

				$postParameters[$name] = '@'.$path;
			}
		}

		$this->curl->setOptions([
			OptionInterface::POST => true,
			OptionInterface::POSTFIELDS => $postParameters
		]);

		return $this->doRequest($url);
	}

	/**
	 * @inheritdoc
	 */
	public function put(string $url, array $postParameters = null): ResponseInterface {
		$postParameters = $postParameters ?? [];

		$this->curl->setOptions([
			OptionInterface::CUSTOMREQUEST => 'PUT',
			OptionInterface::HTTPHEADER => ['Content-Length: '.strlen($postParameters)],
			OptionInterface::POSTFIELDS => $postParameters
		]);

		return $this->doRequest($url);
	}

	protected function doRequest(string $url) {
		$this->curl->setOption(OptionInterface::URL, $url);

		$this->logger->debug('Start request to: '.$url);

		try {
			$result = $this->curl->exec();
		} catch (CurlException $e) {
			$error = $this->curl->error();
			$this->logger->debug('Finish request with error ('.$error->code().'): '.$error->message());

			if ($error->code() == 0) {
				$this->logger->debug('Произошло исключение, но при этом curl_errno говорит, что ошибки в работе curl не было');
				throw new RequestException($e->getMessage(), $e->getCode(), $e);
			}

			$this->errorMapper->throwExceptionByError($error);
		}
		return $this->getResponse($result);
	}

	protected function getResponse(string $body): ResponseInterface {
		$contentType = $this->getInfo(CURLINFO_CONTENT_TYPE);
		$headerLength = $this->getInfo(CURLINFO_HEADER_SIZE);

		$this->logger->debug('RequestHeaders: '."\n".$this->getInfo(CURLINFO_HEADER_OUT));
		$this->logger->debug('RequestTime: '."\n".
			"\t Total: ".$this->getInfo(CURLINFO_TOTAL_TIME)."s\n".
			"\t NameLookup: ".$this->getInfo(CURLINFO_NAMELOOKUP_TIME)."s\n".
			"\t Connect: ".$this->getInfo(CURLINFO_CONNECT_TIME)."s\n".
			"\t BeforeSend: ".$this->getInfo(CURLINFO_STARTTRANSFER_TIME)."s\n".
			"\t RedirectTime: ".$this->getInfo(CURLINFO_REDIRECT_TIME)."s\n"
		);
		$this->logger->debug('RequestInfo: '."\n".
			"\t SizeUpload: ".$this->getInfo(CURLINFO_SIZE_UPLOAD)."\n".
			"\t SpeedUpload: ".$this->getInfo(CURLINFO_SPEED_UPLOAD)."\n".
			"\t SizeDownload: ".$this->getInfo(CURLINFO_SIZE_DOWNLOAD)."\n".
			"\t SpeedDownload: ".$this->getInfo(CURLINFO_SPEED_DOWNLOAD)."\n\n".
			"\t ResponseHeaderSize: ".$headerLength."\n".
			"\t ResponseContentSize: ".$this->getInfo(CURLINFO_CONTENT_LENGTH_DOWNLOAD)."\n".
			"\t ResponseContentType: ".$contentType."\n"
		);

		$responseHeaders = explode("\n", substr($body, 0, $headerLength));
		$body = substr($body, $headerLength);
		$headers = [];
		$httpStatusLine = '';
		$httpVersion = null;
		$httpStatus = null;
		$httpReason = '';
		while (true) {
			if (count($responseHeaders) == 0) {
				break;
			}

			$headerString = array_shift($responseHeaders);

			if ( preg_match('/^HTTP\/([1-2]\.[0-9]{1,})\s([1-5][0-9]{2,2})\s(.*)$/', $headerString, $parts) ) {
				$httpStatusLine = $headerString;
				$httpVersion = $parts[1];
				$httpStatus = $parts[2];
				$httpReason = $parts[3];
				$headers = [];
				continue;
			}

			if (trim($headerString) == '') {
				continue;
			}

			if ( !preg_match('/^([^\:]{2,})\:(.*)$/', $headerString, $headerParts) ) {
				continue;
			}
			$headers[$headerParts[1]] = trim($headerParts[2]);
		}

		$this->logger->debug('Response: '.$httpStatusLine);

		$response = new Response('php://memory', $httpStatus, $headers);
		$response->getBody()->write($body);
		$response->withStatus($httpStatus, $httpReason);
		$response->withProtocolVersion($httpVersion);

		return $response;
	}

	protected function getInfo(string $option) {
		if ( $this->canToggleCurlLogger ) {
			$this->curl->setLogger(new NullLogger());
		}
		$result = $this->curl->getInfo($option);

		if ( $this->canToggleCurlLogger ) {
			$this->curl->setLogger($this->logger);
		}
		return $result;
	}
}
